import 'network.dart';
import '../model/photography.dart';
import 'package:testapplicationjson/constants.dart';

class ListInformation {

  Future<List<PhotographyModel>> getParsedJson() async {
    Network network = Network();
    var result = network.getData();
    List <PhotographyModel> listPhotography = List<PhotographyModel>();
    for (int i = 0; i < kNumberOfElements; i++) {
      listPhotography.add(PhotographyModel(await result, i));
    }
    return listPhotography;
  }
}