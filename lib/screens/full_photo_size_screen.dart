import 'package:flutter/material.dart';

class FullPhotoSizeScreen extends StatelessWidget {

  FullPhotoSizeScreen({this.imageURL});

  final String imageURL;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: Container(
            width: double.infinity,
            height: double.infinity,
            child: Image.network(imageURL),
          ),
        ),
      ),
    );
  }
}
