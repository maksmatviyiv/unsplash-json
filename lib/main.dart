import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'service/list_information.dart';
import 'screens/full_photo_size_screen.dart';
import 'constants.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: ScreenExample());
  }
}

class ScreenExample extends StatefulWidget {
  @override
  _ScreenExampleState createState() => _ScreenExampleState();
}

class _ScreenExampleState extends State<ScreenExample> {
  Future<List> getResult() async {
    ListInformation listInformation = ListInformation();
    var finish = await listInformation.getParsedJson();
    return finish;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
          future: getResult(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Center(
                  child: SpinKitDoubleBounce(
                color: Colors.black,
              ));
            } else {
              return ListView.builder(
                  itemCount: kNumberOfElements,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            child: GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              FullPhotoSizeScreen(
                                                imageURL: snapshot
                                                    .data[index].imageURL,
                                              )));
                                },
                                child: Image.network(
                                    snapshot.data[index].imageURL)),
                            width: 100.0,
                            height: 100.0,
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              TextSpanBlock(
                                  label: 'User Name: ',
                                  textData: snapshot.data[index].userName),
                              SizedBox(
                                height: 15.0,
                              ),
                              TextSpanBlock(
                                  label: 'Image description: ',
                                  textData: snapshot.data[index].namePhoto),
                            ],
                          ),
                        ],
                      ),
                    );
                  });
            }
          }),
    );
  }
}

class TextSpanBlock extends StatelessWidget {
  final String label;
  final String textData;

  TextSpanBlock({this.label, this.textData});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 215.0,
      child: RichText(
        overflow: TextOverflow.fade,
        textAlign: TextAlign.left,
        text: TextSpan(children: [
          TextSpan(text: label, style: kLabelText),
          TextSpan(
            text: textData,
            style: TextStyle(color: Colors.black),
          ),
        ]),
      ),
    );
  }
}
