import 'package:flutter/cupertino.dart';

class PhotographyModel {
   String userName;
   String imageURL;
   String namePhoto;

   PhotographyModel(List data, int number) {
    userName = data[number]['user']['username'];
    namePhoto = data[number]['alt_description'];
    imageURL = data[number]['urls']['small'];
  }
}